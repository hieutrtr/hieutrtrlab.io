---
title: Goroutine Fundamental Explanation
date: 2017-03-11
template: post.jade
---

### What is thread ?
* At application level, thread is a task in an application. An application can use many threads to divide itself to many parts and execute concurrently. An application alway has at least one thread as main thread.
* At fundamental level, Thread is an execution context which containing a set of values that the CPU need for executing an instruction

### Thread and CPU
* CPU or processor alway has various kinds of register depend on architectural register. In programming, we will focus on general purpose register like instruction register.
* CPU registers will be scheduled to access values in a thread by CPU for executing current task.
* Ex : An application has a task of traversing an array to find an element with specific value, that task need to know addresses and values of the array, iterator of the array and a variable to store the value. In this context, CPU have to take enough registers to access the addresses and values in the thread that is allocated for that task.

### Multiple threads
* At application level, multi-threading is a technique separates the application into multiple parts simultaneously for executing concurrently with shared data between the parts.
* At fundamental level, multi-threading is communicating by shared memory between threads.
* There is a mechanism called context switch, it is a process of storing and restoring the state (or execution context) of a processor or thread.
* Processors context switch is more expensive than thread context switch due to a certain amount of time for doing the administration such as saving and loading register and memory maps, updating various tables and lists.
* Thread context switch is usually analogous context switch between user thread, notably green thread is very lightweight, saving and restoring minimal context.

### Goroutine mutiplexing OS thread
* Mutiplexing OS thread is mechanisms of using thread at user level. User thread level can help preventing tons of subtle race conditions such as seeing a partial-copied structure, or needing to lock most data structures.
* There's really what wrong with java and C++, their threads are in OS and they inherit all the problems that they have in OS is memory management system (smallest size of thread allocation)
* There are many kinds of mechanisms such as coroutine in C, fibers in ruby, erlang process and goroutine in golang. They are can be called in term green thread.
* Usually the programming languages which are managing thread at user level or language level cannot be used to build multi-core application. But it's not full of truth, there're golang and erlang which is managing their threads magically move around multiple cores and never be blocked by syscall ( golang scheduler will allocate another OS thread to solve the blocking ).* Goroutine is no need to provide max size of stack, goroutine's stack can grow automatically as needed. One consequence is that goroutine does not require much memory (4-8kb)
* Goroutine scheduler is M:N threading model which is an idea from N:1 model of old-fashion green thread and 1:1 model that just like POSIX Thread in C. M:N model has abilities of both of another models are fast context switching and multi-cores usage. The main disadvantage of this model is the complexities it added to scheduler.
* Goroutine scheduler has a global runqueue and a local runqueue for each context. A context will be mapped with an OS thread by it's local processor. When a syscal is caused in a context and block the OS thread, scheduler

### Asynchronous

### NodeJS vs Go

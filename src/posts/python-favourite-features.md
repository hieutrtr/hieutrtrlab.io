---
title: Python - My favorite features
date: 2017-03-13
template: post.jade
---
Python is a simplest language as i know, I used Python to build many of POC project due to it’s general purposes and multiple paradigm. It’s mean i can develop OO, Functional and Imperative programming only just with Python. Further more, Data Structures of Python is native high-level such as list, tuple, dictionary, collection.... The only thing i consider is Python is slow due to dynamically typed, use interpreter and everything in python is object.

#### High level native data structure
* List : One of most powerful of Python, the implementation is quite simple, internally, CPython, Jython, IronPython use array, ArrayList and , C# list respectively. Expanding of list size is automatically, list alway have capacity that bigger than it's size to keep avoid complexity when add new elements.
* List Comprehensions : The ways to create lists naturally or mathematically. The syntax is including for loop and range with many styles of playing. Belows are some copied example :
```
S = [x**2 for x in range(10)] => [0,1,4,9,16,25,36,49,64,81]
V = [2**i for i in range(13)] => [1,2,4,8,16,32,64,128,256,512,1024,2048,4096]
M = [x for x in S if x%2==0]  => [0,4,16,36,64]
noprimes = [j for i in range(2,8) for j in range(i*2,50,i)]
primes = [x for x in range(2,50) if x not in noprimes]
```
* Tuple : Quite similar to lists, the main difference of them are tuple is immutable. I called tuple is immutable list.
```
Grouping data :
record = (“Mikael”, 1985, “New York”, 2009)
Assignment :
(name,birth_year,hometown,graduated) = record
Single element :
single = (“hello”,) // require the comma
```
* Sets : is a simplest possible container, Set contain unique element without particular order. Set have a huge set of operations for unions.
* Dictionary : Yet another kind of compound type.
```
Interesting examples:
maxtrix = {(0,3):1,(2,1):2,(4,3):3}
maxtrix can be accessed like :
maxtrix[(0,3)]
1
Fibo :
alreadyknow = {0:0,1:1}
def fib(n):
    if n not in alreadyknow:
        new_val = fib(n-1) + fib(n-2)
        alreadyknow[n] = new_val
    return alreadyknow[n]
```

#### virtualenv :
* Installation
```
$ pip install virtualenv
$ virtualenv --version
```
* Create an isolated environment for python application.
```
$ cd my_project_dir
$ virtualenv my_project
$ virtualenv -p /usr/bin/python2.7 my_project
```
* Create a folder inside project folder contain python executable files and pip library use to install another packages.
* Can active and deactive.
```
$ source my_project/bin/active
$ deactive
```
* Keep your environment consistent. Good idea to freeze it.
```
pip freeze > requirements.txt
```
* The requirements can be restored easily
```
pip install -r requirements.txt
```
#### Decorator
#### Streaming
#### Lambda
#### Anti-pattern and multiparadigm languages

### Question during blog :
- Why is Python slow ?
- Multiprocessing vs channel in sharing state.

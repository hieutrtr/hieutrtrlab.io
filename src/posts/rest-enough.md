---
title: REST - Know enough to live
date: 2017-03-19
template: post.jade
---
REST is a architectural style and principles of web application. On the other hand, REST is a set of conventions about how to use HTTP.

* Principles of REST does not required the client to know anything about the structure of API.
* The server need to provide whatever information the client needs to interact with the service.
* The server specifies the location of resource and required fields.
* The browser (or client) does not know in advance where to submit the information and what information to submit. Both form of the informations are provided by the server.

#### HATEOAS (Hypermedia As The Engine Of Application State)
* The HATEOAS is a constraint of RESTful application architecture that distinguishes it from most other network application network architectures. In this constraint, a client interacts with a network application entirely through the hypermedia that is provided by server.
* A REST client needs no prior knowledge about how to interact with any particular application or server beyond a generic understanding of hypermedia.
* The HATEOAS constraint decoupling client and server in a way that allows the server functionaly to evolve independently.
* By contrast, in some SOA, clients interacts with servers through a fixed interface which is shared in the document or interface description language (IDL).

#### Rules of REST
* Offer access through resources.
* Represent resource by representation.
* Exchange seft-descriptive messages.
* Connect resources through links.

#### HTTP (Hypertext Transfer Protocol)
* HTTP is application protocol of distributed, collaboration and hypermedia information systems. It is a foundation of data communication for the World Wide Web.
* HTTP is oriented around resources and verbs. Two verbs in mainstream usage are GET and POST. The HTTP standard defines several others such as PUT and DELETE. These verbs are applied to the resources according to the instructions that are provided by server.
* HTTP relies on two lower order layers. TCP and IP.

#### HTTP steps
Parse URL -> create TCP connection -> send request -> receive response -> keep connection.

#### Vesus SOAP
* SOAP is protocol and REST is architectural style.
Main difference of them is the coupling between client and server. SOAP has a rigid contrast that expected to be break if there's any change of either sides. REST client is often a browser as a generic client .
* SOAP client need to know everything before interact with server. REST client need no prior knowledge about server excepts entrypoint and data type.
* SOAP is sticked with XML but REST has many message formats such as plain text, JSON and XML…
* SOAP is stateful and REST is stateless.

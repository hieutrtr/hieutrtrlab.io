---
title: Purely Functional Programming
date: 2017-04-26
template: post.jade
---
#### Purely functional programming : is a programming paradigm.
* Treats all computation as the evaluation of mathematical functions
* Forbiding changing-states and mutable.
* Purely functional data structures are persistent : Stack, Queue, Deque, Set, Red-black tree or search tree, priority queue (brodal queue).

#### Concept of Functional Programming :
* First-class functions (Function is first-class citizen): pass as argument, return from a function, modified, assigned.
* Higher-order functions : take one function as its argument, returns a function as its result.

#### Non-pure functional programming:
* Using techniques of imperative paradigm such as arrays or i/o methods to achieve first-class function.

#### Properties of purely functional programming
* Strict vs non-strict evaluation : Program always return the same value whichever orders of computation. Mean programmers need not to care about order of computations in program. Eager evaluation may terminate when lazy evaluation can halts.
* Parallel computing : Purely functional programming simplifies to parallel when different parts of evaluation never interact.
* Data structure : immutable, thread-safety, use of lazy evaluation and memoization.
* Purely functional programming can be written in non-purely functional languages

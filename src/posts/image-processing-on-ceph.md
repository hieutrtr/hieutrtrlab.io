---
title: Image Processing base on Ceph Object Storage
date: 2017-03-04
template: post.jade
---

Normally, media data usually store on disk in raw form, this old-fashion solution cause hardness in back-up data (wasting time and not continuous), access slowly and distributed problems.

Ceph is a on of distributed storage solution (comparable with Gluster of Google). Let me introduce a little about two popular ways of using Ceph.

* Object Storage (comparable with S3 of AMZ): In summary, this is a solution to store non-structured data suitable with object such as image, video, file, document… . The object storage solution advantages are scaling, failover and back-up.
* Block Storage (comparable with EBS of AMZ):  In summary, this is a solution to store structured data as hierarchy data and SQL database. The block storage solution advantages in persistent and fast access as a mount disk.

In this playground, i will fork an image processing open source imaginary (written in Golang) of h2non to my imaginary repo. The forked repo is added Ceph connection for upload and get object by Pool and ObjectID (You can read more about how data stored in Ceph to understand the concept).

Okay, now let’s start.
---
#### Dependencies :
* docker engine
* golang newest version
* libvips
* librados

#### Start a Ceph all in one with ceph/demo image simply as below :
```
docker run -d --net=host -v /etc/ceph:/etc/ceph -e MON_IP=192.168.0.20 -e CEPH_PUBLIC_NETWORK=192.168.0.0/24 ceph/demo
```

__libvips__ : the install script require `curl` and `pkg-config` packages.
```
curl -s https://raw.githubusercontent.com/h2non/bimg/master/preinstall.sh | sudo bash
```

__librados__ :
apt-get install librados-dev

__Ceph__ : We will play with ceph before comming into image processing part.
I wil use go-ceph package to play.

I will explain 3 steps of ceph which are enough for this playgound.
* Create a Pool.
* Open IO context (a connection with the pool).
* Create Objects and their attributes.

```
func main() {
     fmt.Println(rados.Version())
     /// Create Connector
     connector, err := rados.NewConn()
     if err != nil {
          fmt.Println(err)
     }

     // Connect
     connector.ReadConfigFile("/etc/ceph/ceph.conf")             // Specify config
     connector.SetConfigOption("log_file", "/etc/ceph/ceph.log") // Specify log path
     connector.Connect()                                         // Start connection
     defer connector.Shutdown()

     connector.MakePool("media")                    // Step1: create pool
     ioctx, err := connector.OpenIOContext("media") // Step2: Open IO context
     if err != nil {
          fmt.Println(err)
          return
     }
     defer ioctx.Destroy()

     data, dataLen := SomeStupidMethodToGetAnyFile() // Let implement it yourself
     // Step3: Set attribute
     err = ioctx.SetXattr("img_31081907", "data", data)
     if err != nil {
          fmt.Println(err)
          return
     }

     buf := make([]byte, dataLen)
     // BonusStep: Get attribute
     ioctx.GetXattr("img_31081907", "data", buf)
     fmt.Println(data)
}
```

The object in this playground should have schema like - ``{"data":<binary>, "width":<integer>, "height":<integer>}``

__Imaginary__ : An interested open-source image processing in Golang. We can learn how they structure an API with built-in package http/net. The middleware is very clean and can expand easily.

I will take the opportunity to play __"best practice of Golang API implementation"__.

Take a look some files: `imaginary.go, server.go, middleware.go, controllers.go, image.go, errors.go, params.go...` All of them in a same package main, i dont think it's a good practice when putting all file in a package but how they handle the context is very clean.

__imaginary.go__
The main file handle all useful options of an API such as rate limit throttle, authentication, gzip enable …
```
var (
     aAddr              = flag.String("a", "", "bind address")
     aPort              = flag.Int("p", 8088, "port to listen")
     aVers              = flag.Bool("v", false, "Show version")
     aVersl             = flag.Bool("version", false, "Show version")
     aHelp              = flag.Bool("h", false, "Show help")
     aHelpl             = flag.Bool("help", false, "Show help")
     aPathPrefix        = flag.String("path-prefix", "/", "Url path prefix to listen to")
     aCors              = flag.Bool("cors", false, "Enable CORS support")
     aGzip              = flag.Bool("gzip", false, "Enable gzip compression")
     aAuthForwarding    = flag.Bool("enable-auth-forwarding", false, "Forwards X-Forward-Authorization or Authorization header to the image source server. -enable-url-source flag must be defined. Tip: secure your server from public access to prevent attack vectors")
     aEnableURLSource   = flag.Bool("enable-url-source", false, "Enable remote HTTP URL image source processing")
     aEnablePlaceholder = flag.Bool("enable-placeholder", false, "Enable image response placeholder to be used in case of error")
     aAlloweOrigins     = flag.String("allowed-origins", "", "Restrict remote image source processing to certain origins (separated by commas)")
     aMaxAllowedSize    = flag.Int("max-allowed-size", 0, "Restrict maximum size of http image source (in bytes)")
     aKey               = flag.String("key", "", "Define API key for authorization")
     aMount             = flag.String("mount", "", "Mount server local directory")
     aCertFile          = flag.String("certfile", "", "TLS certificate file path")
     aKeyFile           = flag.String("keyfile", "", "TLS private key file path")
     aAuthorization     = flag.String("authorization", "", "Defines a constant Authorization header value passed to all the image source servers. -enable-url-source flag must be defined. This overwrites authorization headers forwarding behavior via X-Forward-Authorization")
     aPlaceholder       = flag.String("placeholder", "", "Image path to image custom placeholder to be used in case of error. Recommended minimum image size is: 1200x1200")
     aHttpCacheTtl      = flag.Int("http-cache-ttl", -1, "The TTL in seconds")
     aReadTimeout       = flag.Int("http-read-timeout", 60, "HTTP read timeout in seconds")
     aWriteTimeout      = flag.Int("http-write-timeout", 60, "HTTP write timeout in seconds")
     aConcurrency       = flag.Int("concurrency", 0, "Throttle concurrency limit per second")
     aBurst             = flag.Int("burst", 100, "Throttle burst max cache size")
     aMRelease          = flag.Int("mrelease", 30, "OS memory release interval in seconds")
     aCpus              = flag.Int("cpus", runtime.GOMAXPROCS(-1), "Number of cpu cores to use")
     aEnableCeph        = flag.Bool("enable-ceph", false, "enable ceph integration")
     aCephConf          = flag.String("ceph-conf", "/etc/ceph/ceph.conf", "path to ceph config")
     aEnableFriendly    = flag.Bool("enable-friendly", false, "enable friendly by services")
     aEnableSafeRoute   = flag.Bool("enable-safe-route", false, "enable safe route")
     aSafeKey           = flag.String("safe-key", "", "secret key to hash URI that is used with enable-safe-route")
     aCpuprofile        = flag.String("cpuprofile", "", "write cpu profile `file`")
     aMemprofile        = flag.String("memprofile", "", "write memory profile to `file`")
     aProfilingTimeout  = flag.Int("prof-timeout", 10, "Time to tracking profiling before termination")
)
```

__server.go__
Simple routing for image processing
```
     image := ImageMiddleware(o)
     mux.Handle(joinImageRoute(o, "/"), image(Origin))
     mux.Handle(joinImageRoute(o, "/resize"), image(Resize))
     mux.Handle(joinImageRoute(o, "/enlarge"), image(Enlarge))
     mux.Handle(joinImageRoute(o, "/extract"), image(Extract))
     mux.Handle(joinImageRoute(o, "/crop"), image(Crop))
     mux.Handle(joinImageRoute(o, "/rotate"), image(Rotate))
     mux.Handle(joinImageRoute(o, "/flip"), image(Flip))
     mux.Handle(joinImageRoute(o, "/flop"), image(Flop))
     mux.Handle(joinImageRoute(o, "/thumbnail"), image(Thumbnail))
     mux.Handle(joinImageRoute(o, "/zoom"), image(Zoom))
     mux.Handle(joinImageRoute(o, "/convert"), image(Convert))
     mux.Handle(joinImageRoute(o, "/watermark"), image(Watermark))
     mux.Handle(joinImageRoute(o, "/info"), image(Info))
```

__middleware.go__ - The funniest
First thanks for higher order function, we can see in __server.go__
```
image := ImageMiddleware(o) // Pass the options into the middleware and return as a handler
mux.Handle(joinImageRoute(o, "/"), image(Origin)) // Using `image` as a handler with passed-in options
mux.Handle(joinImageRoute(o, "/resize"), image(Resize))
mux.Handle(joinImageRoute(o, "/enlarge"), image(Enlarge))
mux.Handle(joinImageRoute(o, "/extract"), image(Extract))
.
.
.
```

_ With higher order function, we can do less code. As those options are passed in the middleware at only one line then create a handler function with only one parameter.

_ ImageMiddleware is a higher-order function of handler that take an operator of image processing and handle a chain of middlewares.
```
ImageMiddleware(o Options) —> function(Operation) -> validateImage -> checkSafeKey -> Middleware -> imageController(o, Operation(fn)), o), o), o)
```

_ Every middleware in the chain can have options as parameter easily.

The techniques of creating middleware are also apply here: *Chaining middleware, Third-party middleware*

#### Adding ceph into imaginary
```
// TODO
```

#### Build back-up sollution with Kafka and Block storage
How to answer if the boss ask you if Ceph cluster get faults and cannot be recovered on all of them.
I have just think about a draft continuous back-up solution:
* write log on kafka with format : service_topic - `{“pool”:”pool_name”, “oid”:”object_id”}`
* there’re some workers run on service's topic for back-up data from ceph to a disk as raw image asynchronously

---
title: Java Data Structure
date: 2017-03-04
template: post.jade
---

After years in java, i found that one of most important key in take expert in java is using data structure at master level. I decide to comeback and play with them to make clear my mind.
Below are some advantages of data structure separating to thread-safe and non thread-safe.

![Data structure chart](https://gitlab.com/hieutrtr/hieutrtr.gitlab.io/raw/cf2f8ea9c14e5bcb72306559fc2ead570a4a74c3/templates/images/javadatastructure.PNG)


## Synchronized ( thread-safe )
* Vector: ramdom access, overload memory increase x2, can contain various data types.

## Unsynchronized ( non thread-safe )
* ArrayList: ramdom access, overload memory increase x0.5.
* LinkedList: Add or remove by iterator.
* ArrayDeque: Ramdom access, use like queue and stack faster, no capability restriction.
* HashSet: unordered, non-duplicated, contain various data types, using map.put internally.
* LinkedHashSet: maintain insert order of elements, backed by doubled-linked list.

## Begin with Vector
As we know that Vector is thread-safe and can ramdon access but the weakness is memory with increase x2 when getting overhead. So why are we using Vector ?
Actually, Vector is a legacy from java 1.0 to java 1.2. When collection framework was created, there’re ArrayList to replace Vector. And if we need synchronization ArrayList, we can use :
```java
ArrayList arrayList = new ArrayList();
List synchList = Collections.synchronizedList(arrayList);
```
So there’s still an example for Vector to play :
```java
import java.util.Vector;

public class VectorPlay {
  public static void main(String[] args) {
    Vector v = new Vector(3,4);
    System.out.println("initial size: " + v.size());
    System.out.println("capacity: " + v.capacity());
    v.addElement(new Integer(1));
    v.addElement(new Integer(1));
    v.addElement(new Integer(1));
    v.addElement(new Integer(1));
    System.out.println("Capacity after four editions: " + v.capacity());
    v.addElement("hello");
    System.out.println("Vector can contain various data: " + v);
  }
}
```

## Next is ArrayList
ArrayList was created to replace Vector in Collection Framework. Performance is better due to non-synchronization and capacity x0.5 instead x2 of Vector. Another point, this can contain duplicated value but just for one type of object. The weakness appears in manipulation because shifting elements when remove another elements.

Example show Performance in ArrayList:

#### Adding and capacity:

Because capacity of ArrayList is hidden behind we will have a function to do it using Field to get declared field in ArrayList class as accessible Field.
```java
import java.lang.reflect.Field;
import java.util.ArrayList;

static int getCapacity(ArrayList<?> l) throws Exception {
        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);
        return ((Object[]) dataField.get(l)).length;
  }
```
We will test 2 ArrayList with list1’s capacity is 5 and list2’s capacity is 10000 as X
```java
public static void main(String[] args) throws Exception {
    ArrayList<String> l1 = new ArrayList(5); // list 1 with 5 elements
    ArrayList<String> l2 = new ArrayList(X); // list 2 with x elements
    System.out.println("\nTest with list 1");
    testCapacityLoad(l1);
    System.out.println("\nTest with list 2");
    testCapacityLoad(l2);
  }
```
The overhead of capacity in ArrayList will take O(n) complexity for mining all elements from old Array to new Array. We will counting the execution time of adding a list with overhead and another with fit capacity in function below:
```java
static void testCapacityLoad(ArrayList<String> l) throws Exception {
    System.out.println("Default capacity: " + getCapacity(l));
    long startTime = System.currentTimeMillis();
    addingEle(l,X);
    long addingTime = System.currentTimeMillis();
    System.out.format("Adding elements from %d to %d: %d\n",startTime, addingTime, (addingTime - startTime));
    System.out.println("Overhead capacity: " + (getCapacity(l) - l.size()));
    System.out.println("After trimming capacity: " + getCapacity(l));
  }
```
The result show that list1 with capacity 5 and need to add 10k elements will get many overhead alway slower than list2 is defined capacity as elements need to be added.
```
Test with list 1
Default capacity: 5
Adding elements from 1488176916621 to 1488176916626: 5
Overhead capacity: 6710
After trimming capacity: 106710

Test with list 2
Default capacity: 100000
Adding elements from 1488176916649 to 1488176916651: 2
Overhead capacity: 0
After trimming capacity: 100000
```
There is full example:
```java
import java.util.ArrayList;
import java.lang.reflect.Field;

class ArrayListPlay {
  public static final int X = 100000;
  public static void main(String[] args) throws Exception {
    ArrayList<String> l1 = new ArrayList(5); // list 1 with 5 elements
    ArrayList<String> l2 = new ArrayList(X); // list 2 with x elements
    System.out.println("\nTest with list 1");
    testCapacityLoad(l1);
    System.out.println("\nTest with list 2");
    testCapacityLoad(l2);
  }

  static void testCapacityLoad(ArrayList<String> l) throws Exception {
    System.out.println("Default capacity: " + getCapacity(l));
    long startTime = System.currentTimeMillis();
    addingEle(l,X);
    long addingTime = System.currentTimeMillis();
    System.out.format("Adding elements from %d to %d: %d\n",startTime, addingTime, (addingTime - startTime));
    System.out.println("Overhead capacity: " + (getCapacity(l) - l.size()));
    System.out.println("After trimming capacity: " + getCapacity(l));
  }

  static int getCapacity(ArrayList<?> l) throws Exception {
        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);
        return ((Object[]) dataField.get(l)).length;
  }

  static void addingEle(ArrayList<String> list, int x) {
    for(int i = 0; i < x; i++) {
      list.add("something to test");
    }
  }
}
```

#### Heapsort for random access performance:
Hmmm … Seem like long time no see Heap Sort in real-life programming. Actually, Heap Sort usually appear in many interview meeting when the interviewer have no question to ask you. But here i think it’s a good practice for testing random access due to build a heap tree from an array.
Ok, let taking the opportunity to remind HeapSort algorithms because it’s good for your next interview too.

First, we will meet Heapify function - the function swap a parent node with the childrend if biggest of them bigger than it. Let's see:
```java
public static void heapify(ArrayList<Integer> list, int i, int n) {
    int left = i*2+1;
    int right = left+1;
    int max = i;
    if (left <= n && list.get(max) < list.get(left)) {
      max = left;
    }
    if (right <= n && list.get(max) < list.get(right)) {
      max = right;
    }

    if (max != i) {
      swap(list,i,max);
      heapify(list,max,n);
    }
  }
```
Then i will explain a little about heap sort - We will need create a heap tree first, it’s just like a binary tree but we have root element (index 0 in array) is biggest. Achieve it by using heapify from the middle of array to beginning. Let imagine that by shifting the max from leaves to root we will get root as biggest node.
Then, by swap the root to the end and ignore it then you continue and keep heapify the new root. The loop is the magic of heapsort you should understand by imagine.
```
public static void heapIT(ArrayList<Integer> list) {
  int n = list.size();
  // Creating heap tree
  for (int i = n/2; i >=0; i--) {
    heapify(list,i,n-1);
  }

  for(int i = n-1; i >=0 ; ){
    swap(list,0,i); // swap root to the end
    i-—; // and ignore it
    heapify(list,0,i); // keep heapify the new root
  }
}
```
Now back to the result of random access in ArrayList and this example can use for any of Data Structure that implement an internal array. You can back to heapify function implementation to see the random access of getting left and right child.
```
Current size of list: 10100
Heap Sort elements from 1488213981136 to 1488213981159: 23 milis
```
Sorting 10100 elements in 23 miliseconds. Let’s play it with LinkedList and see difference. Or i will show you how merge sort suitable with LinkedList in the next part.

Dont forget to check full examples on my [github repo](https://github.com/hieutrtr/java_datastructure_play) if there’s any misunderstand

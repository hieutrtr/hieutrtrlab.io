var metalsmith = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var highlighter = require('highlighter');
var templates  = require('metalsmith-templates');
var permalinks = require('metalsmith-permalinks');
var collections = require('metalsmith-collections');
var define      = require('metalsmith-define');
var pagination  = require('metalsmith-pagination');
var snippet     = require('metalsmith-snippet');
var metalsmithPrism = require('metalsmith-prism');

metalsmith(__dirname)
  .source('src')
  .use(define({
    blog: {
      url: 'http://hieutrtr.gitlab.io',
      title: 'Lazy Tech Blog',
      description: 'The laziest tech blog in the world'
    },
    owner: {
      url: 'http://hieutrtr.gitlab.io',
      name: 'Hieu Tran'
    },
    moment: require('moment')
  }))
  .use(collections({
    posts: {
      pattern: 'posts/*.md',
      sortBy: 'date',
      reverse: true
    }
  }))
  .use(pagination({
    'collections.posts': {
      perPage: 5,
      first: 'index.html',
      path: 'page/:num/index.html',
      template: 'index.jade'
    }
  }))
  .use(markdown({
    gfm: true,
    tables: true,
  }))
  .use(metalsmithPrism({
    decode: true
  }))
  .use(snippet({
    maxLength: 500,
  }))
  .use(permalinks())
  .use(templates({
    engine: 'jade',
    directory: 'templates'
  }))
  .destination('public')
  .build(function (err) {
    if (err) {
      throw err;
    }
  });
